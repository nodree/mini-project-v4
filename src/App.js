import React from "react"
import SignUpPage from "./component/SignUpPage"
import SignInPage from "./component/SignInPage"
import TaskDaskboard from "./component/TaskDaskboard"
import { BrowserRouter, Route } from "react-router-dom"

class App extends React.Component {

  render() {
    return (
      <BrowserRouter>
        <div>
          <Route exact path="/" component={SignUpPage} />
          <Route path='/SignInPage' component={SignInPage} />
          <Route path='/TaskDaskboard' component={TaskDaskboard} />
          {/* <SignUpPage path="/SignUp" />
          <SignInPage path="/SignIn" />
          <TaskDaskboard path="TaskDaskboard" /> */}
        </div>
      </BrowserRouter>
    )
  }
}

export default App