import React from 'react'
import SocialButton from "./SocialButton"
import SignInForm from "./SignInForm"
// import SignUpPage from './SignUpPage'
import "../styles/MainLayout.scss"
import { Link } from 'react-router-dom'

class SignInPage extends React.Component {
  render() {
    return (
      <div className="MainLayout">
        <div className="MainLayout__left-component">
          <div className="MainLayout__left-component--todos">
            <h4>Todos</h4>
          </div>
          <div className="MainLayout__left-component--signin">
            <h2 >Hello Friend!</h2>
            <p>Enter your personal details and start your journey with us</p>
            <Link to="/" exact>
              <button className="signin-btn" > SIGN UP</button>
            </Link>
          </div>
        </div>
        <div className="MainLayout__right-component">
          <h1>Sign in to Task Manager</h1>
          <SocialButton />
          <SignInForm />
        </div>
      </div>
    )
  }
}

export default SignInPage