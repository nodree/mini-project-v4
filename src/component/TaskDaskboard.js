import React, { Component } from "react";
import TopNav from "./TopNav";
import { Route } from "react-router-dom";
import "../styles/DaskboardLayout.scss";
import LeftComponent from "./LeftComponent";
import TodosForm from "./TodosForm";

class TaskDaskboard extends Component {
  
  render() {
    return (
      <Route exac path="/TaskDaskboard">
        <div>
          <TopNav />
          <div className="DaskboardLayout">
            <div className="DaskboardLayout__leftcomponent">
              <LeftComponent />
            </div>
            <div className="DaskboardLayout__rightcomponent">
              <TodosForm />
            </div>
          </div>
        </div>
      </Route>
    );
  }
}
export default TaskDaskboard;
