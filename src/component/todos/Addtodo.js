import React, { Component } from "react";
import "./list.scss";

class Addtodo extends Component {
  state = {
    name: "",
    description: "",
    due_date: "2020-02-21"
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  addNewTodo = async (e) => {
    e.preventDefault()
    try {
      let res = await fetch("https://sukatodo.herokuapp.com/api/v1/tasks/create",{
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + localStorage.getItem("access_token")
          },
          body: JSON.stringify({
            name: this.state.name,
            description: this.state.description,
            due_date: this.state.due_date
          })
        });
      this.setState({ name: "", description: "" });
      await res.json();
      // await this.props.getAll();
    }catch (err) {
      console.log(err);
    }
  };

  render() {
    return (
      <form
        onSubmit={this.addNewTodo}
        style={{
          display: "flex"
        }}
      >
        <input
          type="text"
          name="name"
          onChange={this.onChange}
          value={this.state.name}
          placeholder="name"
          style={{
            flex: "10",
            padding: "5px"
          }}
        />
        <input
          type="text"
          name="description"
          onChange={this.onChange}
          value={this.state.description}
          placeholder="description"
          style={{
            flex: "10",
            padding: "5px"
          }}
        />
        <input
          type="Submit"
          className="btn"
        />
      </form>
      
    );
  }
}

export default Addtodo;
