import React, { Component } from "react";
import TodoItem from "./TodoItem";

class Todo extends Component {
  render() {
    return this.props.todos.map(todo => (
      <TodoItem
        key={todo._id}
        markComplete={this.props.markComplete}
        deleteTodo={this.props.deleteTodo}
        todo={todo}
      />
    ));
  }
}

export default Todo;
