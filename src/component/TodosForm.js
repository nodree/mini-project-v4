import React, { Component } from "react";
import Todo from "./todos/Todo.js";
import Addtodo from "./todos/Addtodo.js";
import axios from "axios";

// const BaseURL = "https://sukatodo.herokuapp.com";

class TodosForm extends Component {
  state = {
    todos: []
  };

  componentDidMount() {
    // this.getDataTask()
    setInterval(this.getDataTask, 3000)
  }
  getDataTask = async () => {
    await axios({
      method: "GET",
      url: "https://sukatodo.herokuapp.com/api/v1/tasks/show?page=1",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("access_token")
      }
    }).then(res => {
      this.setState({
        todos: res.data.data.docs
      });
      console.log(res.data.data);
    });
  }

  //toggle complete
  markComplete = async id => {
    this.setState({
      todos: this.state.todos.map(todo => {
        if (todo.id === id) {
          todo.completed = !todo.completed;
        }
        return todo;
      })
    });
  };

  // markComplete = async id => {
  //   const filtered = this.state.todos.filter(todo => todo._id !== id);

  //   try {
  //     await axios({
  //       method: "PUT",
  //       url: "https://sukatodo.herokuapp.com/api/v1/tasks/" + id,
  //       headers: {
  //         "Content-Type": "application/json",
  //         Authorization: "Bearer " + localStorage.getItem("access_token")
  //       }
  //     });
  //     this.setState({
  //       todos: filtered
  //     });
  //     // this.getDataTask();
  //   } catch (err) {
  //     console.log(err);
  //   }
  // };

  // checkTodo = (todo, event) => {
  //   axios.patch('/todos/' + todo.id, {
  //     title: todo.title,
  //     completed: !todo.completed,
  //   })
  //     .then(response => {
  //       runInAction(() => {
  //         todo.completed = !todo.completed;
  //         const index = this.todos.findIndex(item => item.id === todo.id);
  //         this.todos.splice(index, 1, todo);
  //       });
  //     })
  //     .catch(error => {
  //       console.log(error);
  //     });
  // }

  // Delete Todo
  deleteTodo = async id => {
    const filtered = this.state.todos.filter(todo => todo._id !== id);

    try {
      await axios({
        method: "DELETE",
        url: "https://sukatodo.herokuapp.com/api/v1/tasks/delete/" + id,
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + localStorage.getItem("access_token")
        }
      });
      this.setState({
        todos: filtered
      });
      // this.getDataTask();
    } catch (err) {
      console.log(err);
    }
  };

  // Add Todo
  Addtodo = async (e) => {
    e.preventDefault()
    try {
      let res = await fetch(
        "https://sukatodo.herokuapp.com/api/v1/tasks/create", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + localStorage.getItem("access_token")
        },
        body: JSON.stringify({
          name: this.state.name,
          description: this.state.description,
          due_date: this.state.due_date
        })
      });
      this.setState({ name: "", description: "" });
      await res.json();
      await this.props.getAll();
    } catch (err) {
      console.log(err);
    }
  };

  render() {
    return (
      <div className="App">
        <div className="container">
          <Addtodo reloadTask={this.getDataTask} />
          <Todo
            todos={this.state.todos}
            markComplete={this.markComplete}
            deleteTodo={this.deleteTodo}
          />{" "}
        </div>{" "}
      </div>
    );
  }
}

export default TodosForm;
