import React, { Component } from 'react'
import "../styles/topnav.scss"
import { Route, Link } from 'react-router-dom'



export default class TopNav extends Component {
  render() {
    return (
      <Route>
        <div className="topnav">
          <h4>Todos</h4>
          <Link to="SignInPage">
            <button className="topnav__signout-btn">Sign Out</button>
          </Link>
        </div>
      </Route>
    )
  }
}